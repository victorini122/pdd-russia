//gulpfile.js
'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css'),
    cleanCSS = require('gulp-clean-css'),
    // watch = require('gulp-watch'),
    browserSync = require("browser-sync"),
    plumber = require("gulp-plumber"),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    criticalCss = require('gulp-penthouse'),
    reload = browserSync.reload,
    htmlPartial = require('gulp-html-partial');

var path = {
    build: {
        css: 'dist/assets/css',
        js: 'dist/assets/js',
        fonts: 'dist/assets/fonts',
        html: 'dist/',
        images: 'dist/assets/images',
        json: 'dist/assets/js/json',
        md: 'dist/assets/js/md',
        ico: 'dist/',
    },
    src: {
        style: 'src/css/',
        js: 'src/js/scripts.js',
        partials: 'src/partials/',
        region: 'src/js/regions.js',
        examOnline: 'src/js/examOnline.js',
        examGai: 'src/js/examGai.js',
        rulesAndSigns: 'src/js/rulesAndSigns.js',
        difficult: 'src/js/difficult.js',
        ticket: 'src/js/ticket.js',
        signs: 'src/js/signs.js',
    },
    watch: {
        style: 'src/css/**/*.scss',
        js: 'src/js/**/scripts.js',
        html: 'src/*.html',
        fonts: 'src/fonts/**',
        images: 'src/images/**',
        region: 'src/js/**/regions.js',
        examOnline: 'src/js/**/examOnline.js',
        examGai: 'src/js/**/examGai.js',
        rulesAndSigns: 'src/js/**/rulesAndSigns.js',
        difficult: 'src/js/**/difficult.js',
        ticket: 'src/js/**/ticket.js',
        signs: 'src/js/**/signs.js',
        json: 'src/js/**/*.json',
        md: 'src/js/md/*.md',
        ico: 'src/favicon.ico',
    }
};

function css() {
    return gulp.src(path.watch.style)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cleanCSS())
        .pipe(cssmin())
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream());
}

function js() {
    return (
        gulp
            .src(path.src.js)
            .pipe(uglify())
            .pipe(concat('main.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}

function region() {
    return (
        gulp
            .src(path.src.region)
            .pipe(uglify())
            .pipe(concat('regions.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}
function examOnline() {
    return (
        gulp
            .src(path.src.examOnline)
            .pipe(uglify())
            .pipe(concat('examOnline.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}
function examGai() {
    return (
        gulp
            .src(path.src.examGai)
            .pipe(uglify())
            .pipe(concat('examGai.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}

function rulesAndSigns() {
    return (
        gulp
            .src(path.src.rulesAndSigns)
            .pipe(uglify())
            .pipe(concat('rulesAndSigns.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}
function difficult() {
    return (
        gulp
            .src(path.src.difficult)
            .pipe(uglify())
            .pipe(concat('difficult.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}
function ticket() {
    return (
        gulp
            .src(path.src.ticket)
            .pipe(uglify())
            .pipe(concat('ticket.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}
function signs() {
    return (
        gulp
            .src(path.src.signs)
            .pipe(uglify())
            .pipe(concat('signs.js'))
            .pipe(gulp.dest(path.build.js))
            .pipe(browserSync.stream())
    );
}

function html() {
    return (
        gulp
            .src([path.watch.html])
            .pipe(htmlPartial({
                basePath: path.src.partials
            }))
            .pipe(gulp.dest(path.build.html))
    );
}

function fonts() {
    return (
        gulp
            .src([path.watch.fonts])
            .pipe(gulp.dest(path.build.fonts))
    );
}

function images() {
    return (
        gulp
            .src([path.watch.images])
            .pipe(gulp.dest(path.build.images))
    );
}
function json() {
    return (
        gulp
            .src([path.watch.json])
            .pipe(gulp.dest(path.build.json))
    );
}
function md() {
    return (
        gulp
            .src([path.watch.md])
            .pipe(gulp.dest(path.build.md))
    );
}

function ico() {
    return (
        gulp
            .src([path.watch.ico])
            .pipe(gulp.dest(path.build.ico))
    );
}


function watcher() {

    browserSync.init({
        server: {
            baseDir: "dist"
        },
        open: true,
        notify: false
    });

    gulp.watch([path.watch.style], css).on('change', browserSync.reload);
    gulp.watch([path.watch.js], js).on('change', browserSync.reload);
    gulp.watch([path.watch.region], region).on('change', browserSync.reload);
    gulp.watch([path.watch.examOnline], examOnline).on('change', browserSync.reload);
    gulp.watch([path.watch.examGai], examGai).on('change', browserSync.reload);
    gulp.watch([path.watch.rulesAndSigns], rulesAndSigns).on('change', browserSync.reload);
    gulp.watch([path.watch.difficult], difficult).on('change', browserSync.reload);
    gulp.watch([path.watch.ticket], ticket).on('change', browserSync.reload);
    gulp.watch([path.watch.signs], signs).on('change', browserSync.reload);
    gulp.watch([path.watch.html], html).on('change', browserSync.reload);
    gulp.watch([path.watch.fonts], fonts).on('change', browserSync.reload);
    gulp.watch([path.watch.images], images).on('change', browserSync.reload);
    gulp.watch([path.watch.json], json).on('change', browserSync.reload);
    gulp.watch([path.watch.md], md).on('change', browserSync.reload);
    gulp.watch([path.watch.ico], ico).on('change', browserSync.reload);
}

const watch = gulp.parallel(watcher);
exports.watch = watch;
