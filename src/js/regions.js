jQuery(document).ready(function ($) {

    $.getJSON('assets/js/json/regions.json', function (data) {

        var regions = [],
            codes = [];


        data.sort(function (a, b) {
            return a.code - b.code;
        });

        $.each(data, function (key, val) {

            regions.push("<div class='flex-table row' role='rowgroup'>" +
                "<div class='flex-row first' role='cell'><span>" + val.code + "</span></div>" +
                "<div class='flex-row' role='cell'>" + val.region + "</div>" +
                "<div class='flex-row' role='cell'></div>" +
                "</div>");
        });

        $('#regionCodes').append(regions);

    });

});