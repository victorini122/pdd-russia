jQuery(document).ready(function ($) {

    var tickets = [],
        answers = [],
        userAnswers = [],
        trueAnswers = [],
        questions = [],
        hardQuestions = [];

    $.ajax({
        url: 'assets/js/json/conv_tickets_cd.json',
        type: 'GET',
        dataType: 'json',
        async: false,
        success: function (data) {
            tickets = data;
        }, error: function (req, err) {
            console.log(err);
        }
    });

    $.getJSON('assets/js/json/difficult.json', function (data) {
        questions = data.numbers;

        $.each(questions, function (key, value) {

            $('#list').append('<li data-num="' + (key + 1) + '">' + (key + 1) + '</li>');
            $('#list').find('li[data-num="1"]').addClass('active');

            var ticketNum = parseInt(questions[key].split('_')[0]),
                ticketQuestion = questions[key].split('_')[1],
                searchedQuestion = tickets[ticketNum - 1]['questions'][ticketQuestion - 1],
                answersString = '';

            $.each(searchedQuestion.answers, function (k, v) {

                answersString += '<div class="form-group"><label class="form-container">' + (k + 1) + '. ' + v +
                    '<input name="answer" type="radio" value="' + (k + 1) + '"><span class="checkmark"></span>' +
                    '</label></div>';
            });

            hardQuestions.push(searchedQuestion);
            answers.push(answersString);
            trueAnswers.push(searchedQuestion.true_answer);
        });

        var image;

        if (hardQuestions[0].image === null) {
            image = '';
        } else {
            image = '<img src="' + hardQuestions[0].image + '">';
        }

        $('#ticketNum').text('1');
        $('.ticketBody').html(image + '<h2 class="ticket_ttl">' + hardQuestions[0].title + '</h2>');
        $('.tooltip-info p').text(hardQuestions[0].hint);
        $('.form').html(answers[0]);

    });

    $('.form').on('click', '.form-container', function (e) {
        e.preventDefault();

        var current = parseInt($('.nextQuestion').attr('data-current')),
            list = $('#list');

        $('.form .form-container').css('pointer-events', 'none');

        userAnswers.push(parseInt($(this).find('input').val()));

        if (trueAnswers[current - 1] === userAnswers[current - 1]) {
            $(this).addClass('correct');

            list.find('li[data-num="' + current + '"]').removeClass('active').addClass('correct');
            $('.nextQuestion').attr('data-current', current + 1);

            getNext(current);
            list.find('li[data-num="' + (current + 1) + '"]').addClass('active');

        } else {
            $(this).addClass('wrong');
            list.find('li[data-num="' + current + '"]').removeClass('active').addClass('wrong');

            $('.form input[value=' + trueAnswers[current - 1] + ']').parent().addClass('correct')
                .append('<p>' + hardQuestions[current - 1].hint + '</p>');

            if (trueAnswers.length !== userAnswers.length) {
                $('.ticket').find('.nextQuestion').fadeIn();
                $('#list li').css({'pointer-events': 'none'});
            }

        }

        if (trueAnswers.length === userAnswers.length) {

            clearInterval(x);

            var correct = 0,
                imgName = 'exam_success';

            for (var i = 0; i <= userAnswers.length; i++) {
                if (trueAnswers[i] === userAnswers[i]) {
                    correct++;
                }
            }

            if (correct !== trueAnswers.length) {
                $('#examDone .modal_body').addClass('failed');
                $('#examStatus').text('Билет не сдан!');
                $('#examDone svg').addClass('fail');
                imgName = 'exam_failure';
                if (correct & 1) {
                    imgName = 'exam_full_failure';
                }
            }

            $('#user-correct').text(correct);
            $('#from').text(trueAnswers.length);
            $('#time-left').text($('.head_timer').text());
            $('#examImg').attr('src', 'assets/images/' + imgName + '.png');


            $('#examDone').fadeIn();

        }

    });

    $('.nextQuestion').on('click', function (e) {
        e.preventDefault();

        var current = parseInt($(this).attr('data-current')),
            list = $('#list');

        $(this).attr('data-current', current + 1);
        $(this).fadeOut();
        $('#list li').css({'pointer-events': 'all'});


        list.find('li[data-num="' + (current + 1) + '"]').addClass('active');
        getNext(current);


    });

    $('#list').on('click', 'li', function () {

        var clicked = parseInt($(this).attr('data-num')),
            current = parseInt($('.nextQuestion').attr('data-current'));

        if (clicked < current) {
            getNext(clicked - 1);

            if ($(this).hasClass('correct')) {
                $('.form input[value=' + trueAnswers[clicked - 1] + ']').parent().addClass('correct');
                $('.form .form-container').css('pointer-events', 'none');
            } else if ($(this).hasClass('wrong')) {
                $('.form input[value=' + trueAnswers[clicked - 1] + ']').parent().addClass('correct')
                    .append('<p>' + hardQuestions[clicked - 1].hint + '</p>');
                $('.form input[value=' + userAnswers[clicked - 1] + ']').parent().addClass('wrong');
                $('.form .form-container').css('pointer-events', 'none');
            }

        } else if (clicked === userAnswers.length + 1 && trueAnswers.length !== userAnswers.length) {
            getNext(clicked - 1);
        }

    });

    var x;

    function startTime() {
        var startDate = new Date();

        x = setInterval(function () {

            var thisDate = new Date();
            var t = thisDate.getTime() - startDate.getTime();
            var ms = t % 1000;
            t -= ms;
            ms = Math.floor(ms / 10);
            t = Math.floor(t / 1000);
            var s = t % 60;
            t -= s;
            t = Math.floor(t / 60);
            var m = t % 60;
            t -= m;
            t = Math.floor(t / 60);
            var h = t % 60;

            if (m < 10) m = '0' + m;
            if (s < 10) s = '0' + s;

            $('.head_timer').html(m + ':' + s);
        }, 1000);
    }

    startTime();

    $(document).on('click', '#closeModalExamDone', function (e) {
        e.preventDefault();

        $('#examDone').fadeOut();
    });

    function getNext(current) {
        $('.form-container.checked').removeClass('checked');
        $('.form-container input').prop('checked', false);

        var image;

        if (current !== trueAnswers.length) {
            if (hardQuestions[current].image === null) {
                image = '';
            } else {
                image = '<img src="' + hardQuestions[current].image + '">';
            }

            $('#ticketNum').text(current + 1);
            $('.ticketBody').html(image + '<h2 class="ticket_ttl">' + hardQuestions[current].title + '</h2>');
            $('.tooltip-info p').text(hardQuestions[current].hint);
            $('.form').html(answers[current]);
        }

    }

    $(document).on('click', '#closeModalExamDone', function (e) {
        e.preventDefault();

        $('#examDone').fadeOut();
    });

    $('#examDone').on('click', '.answers-btn', function (e) {
        e.preventDefault();

        var list = $('#list');

        $('#examDone').fadeOut();

        $('.form .form-container').css('pointer-events', 'none');
    });

    $('#examDone').on('click', '.restart-btn', function (e) {
        e.preventDefault();
        window.location.reload();
    });


});

