jQuery(document).ready(function ($) {

    $.getJSON('assets/js/json/pdd_data.json', function (data) {

        var links = data[0].items,
            list = [];

        $.each(links, function (key, val) {

            var text = val.title.split('. ')[1];

            list.push('<li><a href="' + val.link + '">' + text + '</a></li>');

        });

        $('.rulesTabs').html(list);

    });

    $('.trafficList .rulesTabs').on('click', 'a', function (e) {
        e.preventDefault();
        var href = $(this).attr('href'),
            container = $('.trafficRules'),
            active = container.find('a[href="' + href + '"]'),
            prev = active.parent().prev().find('a'),
            next = active.parent().next().find('a');

        $('.trafficList').remove();

        getPageInfo(href);
        container.css({'display': 'table'});
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        active.addClass('active');

        arrowBtns(prev, next);

    });

    $('.trafficRules_aside .rulesTabs').on('click', 'a', function (e) {
        e.preventDefault();
        var href = $(this).attr('href'),
            active = $('.trafficRules_aside .rulesTabs').find('.active'),
            prev = $(this).parent().prev().find('a'),
            next = $(this).parent().next().find('a');

        active.removeClass('active');
        $(this).addClass('active');

        getPageInfo(href);

        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });

        arrowBtns(prev, next);
    });

    $('.trafficRules_main__arrows').on('click', '#prev, #next', function (e) {
        e.preventDefault();

        var currentLink = $(this).attr('href'),
            active = $('.trafficRules_aside .rulesTabs').find('a[href="' + currentLink + '"]'),
            old = $('.trafficRules_aside .rulesTabs').find('.active'),
            prev = active.parent().prev().find('a'),
            next = active.parent().next().find('a');

        old.removeClass('active');
        active.click();

        arrowBtns(prev, next);
    });

    function arrowBtns(prev, next) {
        if (prev.length > 0) {
            $('#prev').text(prev.attr('href').split('_')[0] + '. ' + prev.text()).attr({
                'href': prev.attr('href'),
            }).css({'visibility': 'visible'});
        } else {
            $('#prev').css({'visibility': 'hidden'});
        }

        if (next.length > 0) {
            $('#next').text(next.attr('href').split('_')[0] + '. ' + next.text()).attr({
                'href': next.attr('href'),
            }).css({'visibility': 'visible'});
        } else {
            $('#next').css({'visibility': 'hidden'});
        }
    }

    function getPageInfo(fileName) {
        var md = new Remarkable();

        $('.trafficRules_main__content').html(md.render(file_get_contents('assets/js/md/' + fileName)));
    }

});

function file_get_contents(url) {
    var req = null;
    try {
        req = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            req = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            try {
                req = new XMLHttpRequest();
            } catch (e) {
            }
        }
    }
    if (req == null) throw new Error('XMLHttpRequest not supported');

    req.open("GET", url, false);
    req.send(null);

    return req.responseText;
}