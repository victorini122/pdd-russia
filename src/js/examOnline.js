var ticketsNum = [],
    questionsNum = [];

var random_start = 1;
var random_end = 40;
var random_question_end = 20;

allСycles = 20;

var array1 = [], array2 = [];

for (i = random_start; i <= random_end; i++) {
    array1.push(i)
}
for (i = random_start; i <= random_question_end; i++) {
    array2.push(i)
}

for (countCycles = 1; countCycles <= allСycles; countCycles++) {
    ticketsNum.push(array1.splice(Math.random() * array1.length, 1)[0]);
    questionsNum.push(array2.splice(Math.random() * array2.length, 1)[0]);
}

jQuery(document).ready(function ($) {

    var tickets = [],
        answers = [],
        userAnswers = [],
        trueAnswers = [],
        hints = [];

    $.getJSON('assets/js/json/conv_tickets_abm.json', function (data) {

        for (var i = 0; i < ticketsNum.length; i++) {
            var val = data[ticketsNum[i] - 1]['questions'][questionsNum[i] - 1];

            $('#list').append('<li data-num="' + (i + 1) + '">' + (i + 1) + '</li>');
            $('#list').find('li[data-num="1"]').addClass('active');

            var image,
                answersString = '';

            if (val.image === null) {
                image = '';
            } else {
                image = '<img src="' + val.image + '">';
            }

            $.each(val.answers, function (k, v) {
                answersString += '<div class="form-group"><label class="form-container">' + (k + 1) + '. ' + v +
                    '<input name="answer" type="radio" value="' + (k + 1) + '"><span class="checkmark"></span>' +
                    '</label></div>';
            });


            tickets.push(image + '<h2 class="ticket_ttl">' + val.title + '</h2>');
            answers.push(answersString);
            trueAnswers.push(val.true_answer);
            hints.push(val.hint);

        }

        $('.ticketBody').html(tickets[0]);
        $('.form').html(answers[0]);

        $('#list li').css({'pointer-events': 'none'});

    });

    $('.form').on('click', '.form-container', function () {
        $('.form-container.checked').removeClass('checked');
        $(this).addClass('checked');

        var current = parseInt($('#hiddenCurrent').attr('data-current')),
            list = $('#list');

        list.find('li[data-num="' + current + '"]').removeClass('active').addClass('completed');
        list.find('li[data-num="' + (current + 1) + '"]').addClass('active');

        $('#hiddenCurrent').attr('data-current', current + 1);

        userAnswers.push(parseInt($('.form-container.checked input').val()));

        if (trueAnswers.length === userAnswers.length) {

            endOfTest();

        } else {
            $('.form-container.checked').removeClass('checked');
            $('.form-container input').prop('checked', false);

            $('.ticketBody').html(tickets[current]);
            $('.form').html(answers[current]);
        }
    });

    var x;

    function timer() {
        var endTime = Date.now() + 1200000;
        x = setInterval(function () {

            var currentTime = Date.now();
            var leftTime = endTime - currentTime;

            var min = Math.floor((leftTime / 1000 / 60) << 0);
            var sec = Math.floor((leftTime / 1000) % 60);

            $('#curent-min').html(min);

            if (sec < 10) {
                $('#curent-sec').html('0' + sec);
            } else {
                $('#curent-sec').html(sec);
            }

            if (sec < 0) {
                $('#curent-min').html('00');
                $('#curent-sec').html('00');

                endOfTest();
            }
        }, 1000)
    }

    timer();

    $(document).on('click', '#closeModalExamDone', function (e) {
        e.preventDefault();

        $('#examDone').fadeOut();
    });

    $('#examDone').on('click', '.answers-btn', function (e) {
        e.preventDefault();

        var list = $('#list');

        $('#examDone').fadeOut();

        for (var i = 0; i < trueAnswers.length; i++) {

            if (trueAnswers[i] === userAnswers[i]) {
                list.find('li[data-num="' + (i + 1) + '"]').addClass('correct');
            } else {
                list.find('li[data-num="' + (i + 1) + '"]').addClass('wrong');
            }
        }

        getQuestion(trueAnswers.length - 1);

        $('#list li').css({'pointer-events': 'all'});
        $('.form .form-container').css('pointer-events', 'none');
    });

    $('#examDone').on('click', '.restart-btn', function (e) {
        e.preventDefault();
        window.location.reload();
    });

    $('#list').on('click', 'li', function (e) {
        e.preventDefault();

        var clicked = parseInt($(this).attr('data-num'));

        getQuestion(clicked - 1);

    });

    function getQuestion(current) {

        var btn = $('#list').find('li[data-num="' + (current + 1) + '"]');


        $('.form-container.checked').removeClass('checked');
        $('.form-container input').prop('checked', false);

        $('.ticketBody').html(tickets[current]);
        $('.form').html(answers[current]);

        if (btn.hasClass('correct')) {
            $('.form input[value=' + trueAnswers[current] + ']').parent().addClass('correct');

        } else {
            $('.form input[value=' + trueAnswers[current] + ']').parent().addClass('correct')
                .append('<p>' + hints[current] + '</p>');
            $('.form input[value=' + userAnswers[current] + ']').parent().addClass('wrong');

        }
    }

    function endOfTest() {
        var correct = 0,
            imgName = 'exam_success';

        clearInterval(x);

        for (var i = 0; i < userAnswers.length; i++) {
            if (trueAnswers[i] === userAnswers[i]) {
                correct++;
            }
        }
        if (correct < trueAnswers.length - 2) {
            $('#examDone .modal_body').addClass('failed');
            $('#examStatus').text('Билет не сдан!');
            $('#examDone svg').addClass('fail');
            imgName = 'exam_failure';
            if (correct & 1) {
                imgName = 'exam_full_failure';
            }
        }

        $('#user-correct').text(correct);
        $('#from').text(trueAnswers.length);
        $('#time-left').text($('.head_timer').text());
        $('#examImg').attr('src', 'assets/images/' + imgName + '.png');


        $('#examDone').fadeIn();
    }

});
