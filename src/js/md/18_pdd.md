## 18. Приоритет маршрутных транспортных средств

**18.1.** Вне перекрестков, где трамвайные пути пересекают проезжую часть, трамвай имеет преимущество перед безрельсовыми транспортными средствами, кроме случаев выезда из депо.

**18.2.** На дорогах с полосой для маршрутных транспортных средств, обозначенных знаками [5.11.1](pdd://exampdd.ru/mark_5.11.1) , [5.13.1](pdd://exampdd.ru/mark_5.13.1) , [5.13.2](pdd://exampdd.ru/mark_5.13.2)  и [5.14](pdd://exampdd.ru/mark_5.14) , запрещаются движение и остановка других транспортных средств на этой полосе, за исключением:

- школьных автобусов;

- транспортных средств, используемых в качестве легкового такси;

- транспортных средств, которые используются для перевозки пассажиров, имеют, за исключением места водителя, более 8 мест для сидения, технически допустимая максимальная масса которых превышает 5 тонн, перечень которых утверждается органами исполнительной власти субъектов Российской Федерации - гг.Москвы, Санкт-Петербурга и Севастополя.

На полосах для маршрутных транспортных средств разрешено движение велосипедистов в случае, если такая полоса располагается справа.

Водители транспортных средств, допущенных к движению по полосам для маршрутных транспортных средств, при въезде на перекресток с такой полосы могут отступать от требований дорожных знаков [4.1.1 - 4.1.6](pdd://exampdd.ru/mark_4.1.6), [5.15.1](pdd://exampdd.ru/mark_5.15.1) и [5.15.2](pdd://exampdd.ru/mark_5.15.2) для продолжения движения по такой полосе.

Если эта полоса отделена от остальной проезжей части прерывистой линией разметки, то при поворотах транспортные средства должны перестраиваться на нее. Разрешается также в таких местах заезжать на эту полосу при въезде на дорогу и для посадки и высадки пассажиров у правого края проезжей части при условии, что это не создает помех маршрутным транспортным средствам.

**18.3.** В населенных пунктах водители должны уступать дорогу троллейбусам и автобусам, начинающим движение от обозначенного места остановки. Водители троллейбусов и автобусов могут начинать движение только после того, как убедятся, что им уступают дорогу.