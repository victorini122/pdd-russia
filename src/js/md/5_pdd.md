## 5. Обязанности пассажиров

**5.1.** Пассажиры обязаны:
- при поездке на транспортном средстве, оборудованном ремнями безопасности, быть пристегнутыми ими, а при поездке на мотоцикле - быть в застегнутом мотошлеме;

- посадку и высадку производить со стороны тротуара или обочины и только после полной остановки транспортного средства.

Если посадка и высадка невозможна со стороны тротуара или обочины, она может осуществляться со стороны проезжей части при условии, что это будет безопасно и не создаст помех другим участникам движения.

**5.2.** Пассажирам запрещается:
- отвлекать водителя от управления транспортным средством во время его движения;

- при поездке на грузовом автомобиле с бортовой платформой стоять, сидеть на бортах или на грузе выше бортов;

- открывать двери транспортного средства во время его движения.