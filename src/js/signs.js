jQuery(document).ready(function ($) {

    var searchString = 'first-load';

    searchJSON(searchString);

    $('.search').on('click', function (e) {

        e.preventDefault();
        $('.searchBar').removeClass('close');
        $('.searchBar').addClass('open');

    });

    $('.close-search').on('click', function (e) {

        e.preventDefault();

        $('.searchBar').removeClass('open');
        $('.searchBar').addClass('close');

        $('.signs_info__content, .tabs-menu').html('');

        var searchString = 'first-load';
        searchJSON(searchString);

    });

    $('#sign-search').on('keyup', function () {
        var str = $(this).val();
        if (str.length >= 2) {
            searchJSON(str);
        }
    });

    function searchJSON(searchString) {

        $.getJSON('assets/js/json/conv_marks.json', function (data) {

            var searchResult = '';
            $.each(data, function (key, val) {

                if (searchString === 'first-load') {
                    $('.signs_info .tabs-menu').append('<a href="#anchor-' + val.number + 'sign" data-tab="anchor-' + val.number + 'sign" class="b-nav-tab">' +
                        val.number + ' ' + val.title + '</a>');
                    var info = infoHTMLGenerator(val, searchString );
                    $('.signs_info__content').append('<div id="anchor-' + val.number + 'sign"  class="b-tab">' + info + '   </div>');

                    $('.signs_info__content').find('.b-tab').first().addClass('active');
                    $('.signs_info .tabs-menu').find('.b-nav-tab').first().addClass('active');
                } else {
                    var info = infoHTMLGenerator(val, searchString);

                    if (info !== '') {
                        searchResult += '<div class="search-tab">' + info + '   </div>';
                    }

                }

            });
            if (searchString !=='first-load' && searchString.length >= 2) {
                if (searchResult !== '') {
                    $('.signs_info .tabs-menu').html('');
                    $('.signs_info__content').html(searchResult);
                } else {
                    $('.signs_info .tabs-menu').html('');
                    $('.signs_info__content').html('<div class="notFound">' +
                        '<img src="assets/images/not-found.svg">' +
                        '<h4>Ничего не найдено</h4>' +
                        '<p>Введите свой запрос <br>более корректно </p></div>');
                }
            }

            var connectTabs = new Tabs();
        });
    }

    function infoHTMLGenerator(val, searchString) {
        var info = '';

        $.each(val.items, function (k, v) {
            var imgs = '';

            if (v.title.indexOf(searchString) > -1 || searchString === 'first-load') {

                $.each(v.pictures, function (img, src) {
                    imgs += '<img src="' + src + '">';
                });

                info += '<div class="signs_item"><div class="signs_item__img">' + imgs + '</div><div class="signs_item__txt"><h3>' +
                    v.title + '</h3><p>' + v.text.replace(/\n/g, '<br>') + '</p></div></div>';
            }

        });

        return info;
    }
});