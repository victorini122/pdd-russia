var ticketsNum = [],
    questionsNum = [];

var random_start = 1;
var random_end = 40;
var random_question_end = 20;

allСycles = 20;

var array1 = [], array2 = [];

for (i = random_start; i <= random_end; i++) {
    array1.push(i)
}
for (i = random_start; i <= random_question_end; i++) {
    array2.push(i)
}

for (countCycles = 1; countCycles <= allСycles; countCycles++) {
    ticketsNum.push(array1.splice(Math.random() * array1.length, 1)[0]);
    questionsNum.push(array2.splice(Math.random() * array2.length, 1)[0]);
}

jQuery(document).ready(function ($) {

    var tickets = [],
        answers = [],
        userAnswers = [],
        trueAnswers = [],
        dashboard = '';

    $.getJSON('assets/js/json/conv_tickets_abm.json', function (data) {

        for (var i = 0; i < ticketsNum.length; i++) {
            var val = data[ticketsNum[i] - 1]['questions'][questionsNum[i] - 1];

            $('#list').append('<li data-num="' + (i + 1) + '">' + (i + 1) + '</li>');

            var image,
                answersString = '';

            if (val.image === null) {
                image = '';
            } else {
                image = '<img src="' + val.image + '">';
            }

            $.each(val.answers, function (k, v) {
                answersString += '<div class="form-group"><label class="form-container">' + (k + 1) + '. ' + v +
                    '<input name="answer" type="radio" value="' + (k + 1) + '">' +
                    '</label></div>';
            });

            dashboard += '<li data-num="' + (i + 1) + '"><span class="gai-number">' + (i + 1) + '</span>' + (image.length > 0 ? image : '<p>' + val.title + '</p>') + '</li>';
            tickets.push(image + '<h2 class="ticket_ttl">' + val.title + '</h2>');
            answers.push(answersString);
            trueAnswers.push(val.true_answer);
        }

        $('.gai_body').html('<ul>' + dashboard + '</ul>');

    });

    $(document).on('click', '.form .form-container', function (e) {
        e.preventDefault();

        var current = parseInt($('#hiddenCurrent').attr('data-current')),
            list = $('#list');

        $('.form .form-container').css('pointer-events', 'none');

        userAnswers[current - 1] = parseInt($(this).find('input').val());

        var trueLength = userAnswers.reduce(function (acc, cv) {
            return cv ? acc + 1 : acc;
        }, 0);

        if (trueAnswers.length === trueLength) {

            endOfTest();

        } else {
            $('.form-container.checked').removeClass('checked');
            $('.form-container input').prop('checked', false);

            $('#list, .gai_question').hide();
            $('.gai_body').show();
            $('#ticketNum').html('');

            $('.gai_body, #list').find('li[data-num="' + current + '"]').addClass('completed');
        }

    });

    var x;

    function timer() {
        var endTime = Date.now() + 1200000;
        x = setInterval(function () {

            var currentTime = Date.now();
            var leftTime = endTime - currentTime;

            var min = Math.floor((leftTime / 1000 / 60) << 0);
            var sec = Math.floor((leftTime / 1000) % 60);

            $('#curent-min').html(min);

            if (sec < 10) {
                $('#curent-sec').html('0' + sec);
            } else {
                $('#curent-sec').html(sec);
            }

            if (sec < 0) {
                $('#curent-min').html('00');
                $('#curent-sec').html('00');

                endOfTest();
            }
        }, 1000)
    }

    timer();


    $('.gai_body, #list').on('click', 'li', function (e) {
        e.preventDefault();

        var clicked = parseInt($(this).attr('data-num'));

        getQuestion(clicked - 1);

    });

    function getQuestion(current) {

        $('#hiddenCurrent').attr('data-current', current + 1);

        $('.form-container.checked').removeClass('checked');
        $('.form-container input').prop('checked', false);

        $('.gai_body').hide();
        $('#list, .gai_question').show();

        $('#ticketNum').html('Билет №' + ticketsNum[current]);
        $('.gai_question').html(tickets[current] + '<form class="form">' + answers[current] + '</form>');

        if (userAnswers[current]) {
            $('.form input[value=' + userAnswers[current] + ']').parent().addClass('completed');
            $('.form .form-container').css('pointer-events', 'none');
        }

        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });

    }

    function endOfTest() {

        var correct = 0,
            ttl = 'ЭКЗАМЕН СДАН', className = 'success';

        clearInterval(x);

        for (var i = 0; i < userAnswers.length; i++) {
            if (trueAnswers[i] === userAnswers[i]) {
                correct++;
            }
        }

        if (correct < trueAnswers.length - 2) {
            ttl = 'ЭКЗАМЕН НЕ СДАН';
            className = 'failed'
        }

        $('#list, .gai_question').hide();
        $('.gai_body').show();
        $('.gai').addClass(className).append('<div id="result">' + ttl + '</div>');
    }

});
