jQuery(document).ready(function ($) {

    var checkedTicket = localStorage.getItem('ticket_num'),
        currCat, currNum;

    var tickets = [],
        answers = [],
        userAnswers = [],
        trueAnswers = [],
        hints = [];

    if (checkedTicket.split('-')[0] === 'abm') {
        currCat = 'abm';
    } else {
        currCat = 'cd';
    }
    currNum = parseInt(checkedTicket.split('-')[1]);

    $('#ticketNum').text(currNum);

    $.getJSON('assets/js/json/conv_tickets_' + currCat + '.json', function (data) {

        var first = data[currNum - 1];

        $.each(first['questions'], function (key, val) {

            $('#list').append('<li data-num="' + val.number + '">' + val.number + '</li>');
            $('#list').find('li[data-num="1"]').addClass('active');

            var image,
                answersString = '';

            if (val.image === null) {
                image = '';
            } else {
                image = '<img src="' + val.image + '">';
            }

            $.each(val.answers, function (k, v) {
                answersString += '<div class="form-group"><label class="form-container">' + (k + 1) + '. ' + v +
                    '<input name="answer" type="radio" value="' + (k + 1) + '"><span class="checkmark"></span>' +
                    '</label></div>';
            });


            tickets.push(image + '<h2 class="ticket_ttl">' + val.title + '</h2>');
            answers.push(answersString);
            trueAnswers.push(val.true_answer);
            hints.push(val.hint);

        });

        $('.ticketBody').html(tickets[0]);
        $('.form').html(answers[0]);
        $('.tooltip p').text(hints[0]);

    });

    $('.form').on('click', '.form-container', function (e) {
        e.preventDefault();

        var current = parseInt($('#hiddenCurrent').attr('data-current')),
            list = $('#list');

        $('.form .form-container').css('pointer-events', 'none');

        userAnswers[current - 1] = parseInt($(this).find('input').val());

        var trueLength = userAnswers.reduce(function (acc, cv) {
            return cv ? acc + 1 : acc;
        }, 0);

        if (trueAnswers[current - 1] === userAnswers[current - 1]) {
            $(this).addClass('correct');

            list.find('li[data-num="' + current + '"]').removeClass('active').addClass('correct');
            $('#hiddenCurrent').attr('data-current', current + 1);

            getQuestion(current);
            list.find('li[data-num="' + (current + 1) + '"]').addClass('active');

        } else {
            $(this).addClass('wrong');
            list.find('li[data-num="' + current + '"]').removeClass('active').addClass('wrong');

            $('.form input[value=' + trueAnswers[current - 1] + ']').parent().addClass('correct')
                .append('<p>' + hints[current - 1] + '</p>');

            if (trueAnswers.length !== trueLength && current !== trueAnswers.length) {
                $('.ticket').find('.nextQuestion').fadeIn();
            }

        }

        if (trueAnswers.length === trueLength) {

            var correct = 0,
                imgName = 'exam_success';

            for (var i = 0; i <= userAnswers.length; i++) {
                if (trueAnswers[i] === userAnswers[i]) {
                    correct++;
                }
            }

            if (correct !== trueAnswers.length) {
                $('#examDone .modal_body').addClass('failed');
                $('#examStatus').text('Билет не сдан!');
                $('#examDone svg').addClass('fail');
                imgName = 'exam_failure';
                if (correct & 1) {
                    imgName = 'exam_full_failure';
                }
            }

            $('#user-correct').text(correct);
            $('#from').text(trueAnswers.length);
            $('#time-left').text($('.head_timer').text());
            $('#examImg').attr('src', 'assets/images/' + imgName + '.png');

            $('#examDone').fadeIn();

            localStorage.setItem(checkedTicket, correct);
        }

    });

    $('.nextQuestion').on('click', function (e) {
        e.preventDefault();

        var current = parseInt($('#hiddenCurrent').attr('data-current')),
            list = $('#list');

        $('#hiddenCurrent').attr('data-current', current + 1);
        $(this).fadeOut();

        list.find('li[data-num="' + (current + 1) + '"]').addClass('active');
        getQuestion(current);

    });

    var x;

    function startTime() {
        var startDate = new Date();

        x = setInterval(function () {

            var thisDate = new Date();
            var t = thisDate.getTime() - startDate.getTime();
            var ms = t % 1000;
            t -= ms;
            ms = Math.floor(ms / 10);
            t = Math.floor(t / 1000);
            var s = t % 60;
            t -= s;
            t = Math.floor(t / 60);
            var m = t % 60;
            t -= m;
            t = Math.floor(t / 60);
            var h = t % 60;

            if (m < 10) m = '0' + m;
            if (s < 10) s = '0' + s;

            $('.head_timer').html(m + ':' + s);
        }, 1000);
    }

    startTime();

    $(document).on('click', '#closeModalExamDone', function (e) {
        e.preventDefault();

        $('#examDone').fadeOut();
    });

    $('#examDone').on('click', '.answers-btn', function (e) {
        e.preventDefault();

        var list = $('#list');

        $('#examDone').fadeOut();

        for (var i = 0; i < trueAnswers.length; i++) {

            if (trueAnswers[i] === userAnswers[i]) {
                list.find('li[data-num="' + (i + 1) + '"]').addClass('correct');
            } else {
                list.find('li[data-num="' + (i + 1) + '"]').addClass('wrong');
            }
        }

        getQuestion(trueAnswers.length - 1);

        $('#list li').css({'pointer-events': 'all'});
        $('.form .form-container').css('pointer-events', 'none');
    });

    $('#examDone').on('click', '.restart-btn', function (e) {
        e.preventDefault();
        window.location.reload();
    });

    $('#list').on('click', 'li', function (e) {
        e.preventDefault();

        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var clicked = parseInt($(this).attr('data-num'));

        $('#hiddenCurrent').attr('data-current', clicked);

        getQuestion(clicked - 1);

    });

    function getQuestion(current) {

        var btn = $('#list').find('li[data-num="' + (current + 1) + '"]');

        $('.form-container.checked').removeClass('checked');
        $('.form-container input').prop('checked', false);

        $('.ticketBody').html(tickets[current]);
        $('.form').html(answers[current]);

        $('.tooltip p').text(hints[current]);

        if (btn.hasClass('correct')) {
            $('.form input[value=' + trueAnswers[current] + ']').parent().addClass('correct');
            $('.form .form-container').css('pointer-events', 'none');

        } else if (btn.hasClass('wrong')) {
            $('.form input[value=' + trueAnswers[current] + ']').parent().addClass('correct')
                .append('<p>' + hints[current] + '</p>');
            $('.form input[value=' + userAnswers[current] + ']').parent().addClass('wrong');
            $('.form .form-container').css('pointer-events', 'none');
        }
    }

});

