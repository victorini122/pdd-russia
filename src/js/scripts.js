jQuery(document).ready(function ($) {

    $('#menu a').each(function () {
        var link = $(this);

        if (link[0].href === window.location.href) {

            link.addClass('active');

            if (link[0].href.includes('examOnline')) {
                $('#exam-online').addClass('active');
            }
        }

    });


    $('.apps').on('click', '#close', function () {
        $('.apps').fadeOut();
    });

    if ($(document).find('.tabs')) {
        var connectTabs = new Tabs();
    }

    $(document).on('click', '#closeModal', function (e) {
        e.preventDefault();

        $('#contacts').fadeOut();
    });

    $(document).on('click', '#openContacts', function (e) {
        e.preventDefault();

        $('#contacts').fadeIn();
    });


    $(document).on('change keyup keypress keydown', 'textarea', function () {
        if ($(this).val().length > 0) {
            $('#contacts').find('button[type="submit"]').addClass('active');
        }
    });

    $(document).on('click', '#openMenu', function (e) {
        e.preventDefault();
        $('.sidebar header').slideToggle();
    });

    $('.allTickets').on('click', '.col a', function () {
        var ticketNum = $(this).parents('.col').attr('data-ticket');
        localStorage.setItem('ticket_num', ticketNum);
    });

    $.getJSON('assets/js/json/conv_tickets_abm.json', function (data) {

        $.each(data, function (key, val) {

            var resolved = parseInt(localStorage.getItem('abm-' + val.number)),
                completedClass = "",
                btnName = "начать";

            if (!resolved) {
                resolved = 0;
            }

            if (resolved === 20) {
                completedClass = "completed";
                btnName = "заново";
            }

            $('#all, #abm').append('<div data-ticket="abm-' + val.number + '" class="col"><div class="col-wrap ' + completedClass + '">' +
                '<p class="col_num">№' + val.number + '</p>' +
                '<p class="col_qty">' + resolved + '/' + val.questions.length + '</p>' +
                '<p class="col_qty__info">Вопросов решено</p>' +
                '<div class="col_btn"><a class="' + completedClass + '" href="ticket.html">' + btnName + '</a>' +
                '</div></div></div>');
        });
    });

    $.getJSON('assets/js/json/conv_tickets_cd.json', function (data) {

        var length = $('#abm .col').length;

        $.each(data, function (key, val) {

            var resolved = parseInt(localStorage.getItem('cd-' + val.number)),
                completedClass = "",
                btnName = "начать";

            if (!resolved) {
                resolved = 0;
            }

            if (resolved === 20) {
                completedClass = "completed";
                btnName = "заново";
            }

            $('#all').append('<div data-ticket="cd-' + val.number + '" class="col"><div class="col-wrap ' + completedClass + '">' +
                '<p class="col_num">№' + (length + val.number) + '</p>' +
                '<p class="col_qty">' + resolved + '/' + val.questions.length + '</p>' +
                '<p class="col_qty__info">Вопросов решено</p>' +
                '<div class="col_btn"><a class="' + completedClass + '" href="ticket.html">' + btnName + '</a>' +
                '</div></div></div>');

            $('#cd').append('<div  data-ticket="cd-' + val.number + '" class="col"><div class="col-wrap ' + completedClass + '">' +
                '<p class="col_num">№' + val.number + '</p>' +
                '<p class="col_qty">' + resolved + '/' + val.questions.length + '</p>' +
                '<p class="col_qty__info">Вопросов решено</p>' +
                '<div class="col_btn"><a class="' + completedClass + '" href="ticket.html">' + btnName + '</a>' +
                '</div></div></div>');
        });
    });


});

function Tabs() {
    var bindAll = function () {
        var menuElements = document.querySelectorAll('[data-tab]');

        for (var i = 0; i < menuElements.length; i++) {
            menuElements[i].addEventListener('click', change, false);
        }
    }

    var clear = function () {
        var menuElements = document.querySelectorAll('[data-tab]');

        for (var i = 0; i < menuElements.length; i++) {
            menuElements[i].classList.remove('active');
            var id = menuElements[i].getAttribute('data-tab');
            document.getElementById(id).classList.remove('active');
        }
    }

    var change = function (e) {
        e.preventDefault();
        clear();
        e.target.classList.add('active');
        var id = e.currentTarget.getAttribute('data-tab');
        document.getElementById(id).classList.add('active');
    }


    bindAll();
}